
{/*custom code here */

intern = [];
intern['if_else_flag'] = false;
data = [];
data['username'] = 'mehdi';
data['age'] = 18;
vars = [];
vars = data;
}

start =
output:(eval_str) {return output;}

eval_str = 
out:(text) {return out;}

// match {{variable}}
variable_tag =
v:( s1:space? '{{' space? w:var_word space? '}}' s2:space? {return {name:w,s1:s1,s2:s2};})
{
 console.log('rule : variable_tag');
 return (vars[v.name] !== undefined ? (v.s1 || '') +vars[v.name] + (v.s2 || '') : '');
}

variable =
name:(w:var_word {return w;}) {
console.log('vars["' +name + '"] : ' + vars[name]);
if(typeof vars[name] == 'undefined') return '';
value = (typeof vars[name] == 'string' ? '"' + vars[name] + '"' : vars[name]);
return value;
}

array_variable = 
name:(w:var_word {return w;}) 
{
console.log('vars["' +name + '"] : ' + vars[name]);
if(typeof vars[name] !== 'object') return false;
return vars[name];
}


simple_quote = "'"
 
double_quote = '"'


str = 
string:(space? w:(chars / variable_tag) space? {return w;})+
{
 return string.join('');
}

quoted_str =
str:(double_quote str double_quote){
 return str.join('');
}

word = w:letter+ {return w.join('');}

var_word = w:(var_letter)+ {return w.join('');}

chars = c:char+ {return c.join('');}

var_letter = [a-zA-Z_-]

letter = [a-zA-Z]

char = c:(!reserved) c:.  {return c;}

digits = d:digit+ {return parseInt(d.join(''));}

digit = 
d:[0-9] {return parseInt(d);}

// str:(space? w:(instructions_block /variable_tag / word / chars / quoted_str) space? {return w;})* 
text =

str:( s1:space? w:(instructions_block /variable_tag / word / chars / quoted_str) s2:space? {return (s1 || '') +w + (s2 || '')})* 
{
 return str.join('');
}

s = [' '\t\r\n]

space = s+

instructions_block =
expr:open_instructions_tag content:eval_str close_keyword:close_instructions_tag 
{
    if(expr.keyword != close_keyword.keyword){
     return 'wrong close tag line :' + close_keyword.line + ' col : ' + close_keyword.col;
      }
    if(expr.bool)
     return content;
    else return '';
}

open_instructions_tag =
space? '{%' space? kw:keyword space? bool:expression space? '%}' space?
{
  if(kw == 'if' && bool == true)
    intern['if_else_flag'] = true;
  else if(kw == 'if' && bool == false)
    intern['if_else_flag'] = false;
  return {bool : bool, keyword : kw};
}

close_instructions_tag =
space? '{%' space? 'end'i kw:keyword space? '%}' space? {return {keyword:kw, line:line, col:column};}

operand = (c:(double_quote chars+ double_quote){return c.join('');}) / digits / bool / variable

expression = 

elseStatement /

rangeStatement /

( '(' space? left:(operand / expression) space? op:operator space? right:(operand / expression) space? ')' 
{
 return eval(left + op + right);
}) / 
( '(' space? val:(quoted_str / bool / variable) space? ')' 
{
return eval(val);
})

forStatement = 
tmpVariable space? 'in' array_variable
tmpVariable = name:(w:var_word {return w;}) {vars[name] = '';}

elseStatement =
 space? 'do'i space? {
/* return ( intern['if_else_flag'] ? true : false); */ 
console.log(intern['if_else_flag']);
if(intern['if_else_flag'])
  {
   intern['if_else_flag'] = false;
   return false;
  }
 else
 {
   intern['if_else_flag'] = true;
   return true;
 }
}


rangeStatement = 
needle:(digits / variable) space? rangeOperator space? range1:digits space? ('...' / 'et'i / 'and'i) space? range2:digits
{
 if(typeof needle != 'number')
  return false;
return ((needle <= range1 && needle >= range2) || (needle <= range2 && needle >= range1) ? true : false );

}

rangeOperator  = "entre"i / "in"i

bool = 
k:'true' / k:'false' {console.log('bool : ' + k);return (k == 'true' ? true : false);}

keyword =
'if'i /
'else'i /
'for'i /
'si'i {return 'if';} /
'sinon'i {return 'else';}

operator = 
'<=' /
'>=' /
'<' /
'>' /
'===' /
'==' /
'!==' /
'!=' /
'or'i {return '||';}/
'ou'i {return '||';}/
'and'i {return '&&';}/
'et'i {return '&&';}/
'&&' /
'||'


reserved = 
'"' /
"'" /
'{{' /
'}}' /
'{%' /
'%}' /
'new'